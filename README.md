# golang-jwt

Сервис аутентификации на основе JWT
([основан на этом репозитории](https://github.com/adigunhammedolalekan/go-contacts))

API включает в себя 4 метода:
- Создание нового пользователя ```POST api/user/new```
- Аутентификация пользователя ```POST api/user/login```
- Создание нового контакта ```POST api/contacts/new```
- Поиск всех контактов пользователя```GET api/me/contacts```
